import Head from 'next/head'

import Header from "./header"
import Footer from "./footer"

const content = (pros) => {
    return (
        <div>
            <Head>
                <title>{pros.title}</title>
                <link rel="icon" href="/img/startscoLogo.webp" />
            </Head>
            <div id="page">
                <Header/>
                <div>
                    <main className="container">
                        {pros.children}
                    </main>
                </div>
                <Footer/>
            </div>
            <script src="/js/load.js" ></script>
            <style jsx global>
                {`
                    :root{
                        --header-height:60px;

                        --color-1: #fff;
                        --color-2: #e6e6e6;

                        --color-t-1: #202020;
                        --color-t-2: #181818;
                    }
                    body.themeDack{
                        --color-1: #202020;
                        --color-2: #181818;

                        --color-t-1: #fff;
                        --color-t-2: #e4e4e4;

                        --modeDarck-translate : 35px;
                    }
                    #page{
                        background-color:var(--color-1,"#fff");
                        min-height:100vh;
                        padding-top:var( --header-height,60px);
                        color:var(--color-t-2,#909090);
                        display:flex;
                        flex-wrap:wrap;
                    }
                    #page > *{
                        width:100%;
                    }
                    header{
                        background-color:var(--color-2,"#909090");
                    }
                    .container{
                        width:100%;
                        max-width:1000px;
                        margin:auto;
                        padding: 0 15px;
                    }
                    h1,h2,h3,h4,h5,h6{
                        color:var(--color-t-1,#202020);
                    }
                    .uppercase{
                        text-transform:uppercase;
                    }
                    table{
                        width: 100%;
                        border-collapse: collapse;
                    }
                    table,thead,tbody,tfoot,tr,th,td{
                        border: 1px solid;
                    }
                    tr,th,td{
                        padding: 5px 10px;
                    }
                    a{
                        font-weight:700;
                        cursor:pointer;
                    }
                    .overflow_tabla{
                        width:100%;
                        overflow:auto;
                    }
                    .d-block{
                        display:block;
                    }
                    footer{
                        width:100%;
                        background-color:var(--color-2,"#909090");
                        padding: 5px 15px;
                        margin-top:auto;
                        display: flex;
                        flex-wrap:wrap;
                        justify-content: space-between;
                    }
                `}
            </style>
        </div>
    )
}

export default content