import Link from 'next/link'

const header = () =>{
    const toggleTheme = () =>{
        document.body.classList.toggle('themeDack')
        var themaDark = localStorage.getItem( "themaDark" ) == "true"
        themaDark = !themaDark
        localStorage.setItem( "themaDark" ,themaDark )
    }
    return (
        <header>
            <nav>
                <Link href="/">
                    <a>
                        <img src="/img/startscoLogo.webp" width={40}/>
                    </a>
                </Link>
                <Link href="/">
                    <a>
                        Login
                    </a>
                </Link>
                <Link href="/data">
                    <a>
                        Data
                    </a>
                </Link>
                <label
                onClick={toggleTheme}
                htmlFor="modeDarck" 
                className="label_modeDarck"></label>
            </nav>
            <style jsx>
                {`
                    header{
                        position: fixed;
                        top:0;
                        left:0;
                        width:100%;
                        height:var(--header-height,60px);
                        display:flex;
                        align-items:center;
                        padding: 5px 15px;
                    }
                    nav{
                        display:flex;
                        align-items:center;
                        width:100%;
                    }
                    a{
                        display:inline-block;
                        margin-right:15px;
                        color:var(--color-t-1, #202020);
                    }
                    label{
                        margin-left:auto;
                        width:60px;
                        height:30px;
                        display:inline-block;
                        background-color : var(--color-t-1, #202020);
                        border-radius:20px;
                        position:relative;
                        cursor:pointer;
                    }
                    label:before{
                        content:"";
                        width:20px;
                        height:20px;
                        display:inline-block;
                        border-radius:100%;
                        background-color : var(--color-1, #202020);
                        top:5px;
                        left:var(--modeDarck-translate,5px);
                        position:absolute;
                    }
                `}
            </style>
        </header>
    )
}
export default header