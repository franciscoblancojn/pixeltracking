import React, {Component} from "react";
import Router from 'next/router';

class Login extends React.Component{
    state = {
        user : "",
        password : "",
    }
    render(){
        return (
            <div className="contentLogin">
                <form
                onSubmit={this.submit}
                >
                    <label htmlFor="user">
                        <span>
                            User
                        </span>
                        <input
                        id="user"
                        name="user"
                        placeholder="User"
                        type="text"
                        value={this.state.user}
                        onChange={this.handleChange}
                        />
                    </label>
                    <label htmlFor="password">
                        <span>
                            Password
                        </span>
                        <input
                        id="password"
                        name="password"
                        placeholder="Password"
                        type="password"
                        value={this.state.password}
                        onChange={this.handleChange}
                        />
                    </label>
                    <button>
                        Login
                    </button>
                </form>
                <style jsx>
                    {`
                        .contentLogin{
                            width:100%;
                            max-width:400px;
                            margin: auto;
                            padding: 20px 0;
                        }
                        label{
                            display:block;
                            margin-bottom:20px;
                        }
                        label span{
                            display:block;
                            margin-bottom:5px;
                            font-size:20px;
                        }
                        input,
                        button{
                            font-size:20px;
                            width:100%;
                            padding: 10px 15px;
                            border:0;
                            outline:none !important;
                        }
                        button{
                            text-align:center;
                            cursor:pointer;
                            transition: .5s;
                            border: 3px solid var(--color-t-1, #fff);
                        }
                        button:hover{
                            background:var(--color-1, #202020);
                            color:var(--color-t-1, #fff);
                        }
                    `}
                </style>
            </div>
        )
    }
    handleChange = (e) => {
        var field = e.target.id;
        var value = e.target.value;
        this.setState({ [field]: value });
	}
    submit = (e) =>{
        e.preventDefault()
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "type":"isClient",
            "user":this.state.user,
            "password":this.state.password
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://pixeltracking.startscoinc.com/app", requestOptions)
            .then(response => response.text())
            .then(result => {
                console.log(result)
                localStorage.setItem( "user" , this.state.user )
                localStorage.setItem( "password" , this.state.password )
                //Router.push('data')
            })
            .catch(error => console.log('error', error));
    }
}
export default Login;