import Content from "./component/content"
import Login from "./component/login"
const index = () => {
    return (
        <Content title="Aveonline App">
            <Login/>
            <style jsx>
                {`
                .content_cart{
                    display:flex;
                    flex-wrap:wrap;
                    justify-content: space-around;
                }
                .cart{
                    width:200px;
                    height:200px;
                    max-width:100%;
                    border:3px solid;
                    border-radius:10px;
                    margin: 20px;
                    display:flex;
                    justify-content: center;
                    align-items: center;
                    text-align:center;
                    background: var(--color-2);
                    font-size:20px;
                }
                `}
            </style>
        </Content>
    )
}
export default index
